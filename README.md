# Mocking

<p>
 <img src="https://gitlab.com/pascal.cantaluppi/mocking/-/raw/master/img/masks.png" width="100" alt="Masks" />
</p>

## JavaScript Testing - Mocking Async Code

<p>Testing async JavaScript code or testing JS dependencies in general can be difficult.<br /> But "mocking" is a technique that can easily be implemented with Jest to make JavaScript testing a breeze again.</p>

- <a href="https://academind.com/learn/javascript/javascript-testing-mocking-async-code/" target="blank">https://academind.com/learn/javascript/javascript-testing-mocking-async-code/</a>
